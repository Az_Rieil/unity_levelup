После того как я добавлю вас в репозиторий ( и вы подтвердите это через приглашение на почту )

1. Устанавливаем гит https://git-scm.com/downloads
2. Переходим в папку где нужно разместить проект
3. ПКМ -> git bash
4. git clone https://bitbucket.org/Az_Rieil/unity_levelup.git FOLDER_NAME
Выскочет окно где надо будет подтвердить пароль
(FOLDER_NAME заменить на удобное, unity, unity_levelup, и тд )

Материал
1. Основы ветвления
https://git-scm.com/book/ru/v2/%D0%92%D0%B5%D1%82%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5-%D0%B2-Git-%D0%9E%D1%81%D0%BD%D0%BE%D0%B2%D1%8B-%D0%B2%D0%B5%D1%82%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D1%8F-%D0%B8-%D1%81%D0%BB%D0%B8%D1%8F%D0%BD%D0%B8%D1%8F
2. Управления ветками
https://git-scm.com/book/ru/v2/%D0%92%D0%B5%D1%82%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5-%D0%B2-Git-%D0%A3%D0%BF%D1%80%D0%B0%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5-%D0%B2%D0%B5%D1%82%D0%BA%D0%B0%D0%BC%D0%B8
3. Более простым языком на хабре
https://habr.com/ru/post/342116/

Для наших задач нужно понимать команды
add, push, pull, commit, branch, checkout, status

Для каждого ДЗ создавать ветку Task_00_Surname где 00 номер, Surname - фамилия
git checkout -b Task_01_Ivanov
ИЛИ
git branch Task_01_Ivanov
git checkout Task_01_Ivanov

Проверить текущую ветку - git branch
 - Вносите локальные изменения и заливаете их на свою ветку через add & push
git add .
git commit -m "Here I add new features"
git push -u origin HEAD 

Проверить локальные изменения и состояние ветки
git status